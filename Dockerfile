FROM openjdk:8-jre-alpine
COPY target/learning-kafka-1.0-SNAPSHOT.jar /learning-kafka-1.0-SNAPSHOT.jar
CMD ["/usr/bin/java", "-jar", "/learning-kafka-1.0-SNAPSHOT.jar"]
