package jacekolszak;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.kstream.internals.suppress.StrictBufferConfigImpl;

import java.time.Duration;

import static java.time.Duration.ofMillis;
import static org.apache.kafka.streams.kstream.Suppressed.untilWindowCloses;

final class WindowedTopologyFactory {

    private final Duration windowDuration = ofMillis(1000L);
    private final Duration graceDuration = ofMillis(0L);
    private final Serde<String> stringSerde = Serdes.String();

    Topology topology() {
        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> inputStream = builder.stream("input");
        inputStream
                .filter((key, value) -> key.equals("k1"))
                .groupByKey()
                .windowedBy(TimeWindows.of(windowDuration).grace(graceDuration))
                .reduce((value1, value2) -> value1 + value2, Materialized.with(stringSerde, stringSerde))
                .toStream((window, concatenation) -> "")
                .transform(() -> new SuppressingTransformer<>())
                .to("output");
        return builder.build();
    }

}
