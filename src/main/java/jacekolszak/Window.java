package jacekolszak;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

final class Window {

    @JsonProperty
    private final long start;

    Window(long now, long windowDurationMillis) {
        this((now / windowDurationMillis) * windowDurationMillis);
    }

    @JsonCreator
    private Window(@JsonProperty("start") long start) {
        this.start = start;
    }

    long start() {
        return start;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Window window = (Window) o;
        return start == window.start;
    }

    @Override
    public int hashCode() {
        return Objects.hash(start);
    }
}
