package jacekolszak;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.PunctuationType;
import org.apache.kafka.streams.processor.Punctuator;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

final class WindowSuppressingTransfomer<V, R extends KeyValue<Window, V>> implements Transformer<Window, V, R>, Punctuator {

    private final long windowDurationMillis;
    private Window window;
    private ProcessorContext context;
    private final Map cache = new HashMap();

    WindowSuppressingTransfomer(long windowDurationMillis) {
        this.windowDurationMillis = windowDurationMillis;
    }

    @Override
    public void init(ProcessorContext context) {
        this.context = context;
        context.schedule(Duration.ofMillis(windowDurationMillis), PunctuationType.WALL_CLOCK_TIME, this);
    }

    @Override
    public R transform(Window key, V value) {
        ensureWindowStartIsSet();
        System.out.println("transforming " + key + ":" + value);
        cache.put(key, value);
        return null;
    }

    private void ensureWindowStartIsSet() {
        if (window == null) {
            window = new Window(context.timestamp(), windowDurationMillis);
        }
    }

    @Override
    public void close() {
    }

    @Override
    public void punctuate(long now) {
        ensureWindowStartIsSet();
        System.out.println("punctuate with t = " + now);
        System.out.println("millis elapsed = " + (now - window.start()));
        if (now - window.start() > windowDurationMillis) {
            System.out.println("forwarding " + cache.size() + " events");
            cache.forEach((key, value) -> context.forward(key, value));
            cache.clear();
            window = new Window(now, windowDurationMillis);
            System.out.println("starting new window with t=" + window.start());
        }
    }
}
