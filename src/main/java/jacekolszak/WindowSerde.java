package jacekolszak;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

import java.io.IOException;
import java.util.Base64;
import java.util.Map;

final class WindowSerde implements Serializer<Window>, Deserializer<Window>, Serde<Window> {

    private ObjectMapper mapper = new ObjectMapper();

    @Override
    public Window deserialize(String topic, byte[] data) {
        return deserialize(topic, null, data);
    }

    @Override
    public Window deserialize(String topic, Headers headers, byte[] data) {
        try {
            return mapper.readerFor(Window.class).readValue(data);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Serializer<Window> serializer() {
        return this;
    }

    @Override
    public Deserializer<Window> deserializer() {
        return this;
    }

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {

    }

    @Override
    public byte[] serialize(String topic, Window data) {
        return serialize(topic, null, data);
    }

    @Override
    public byte[] serialize(String topic, Headers headers, Window data) {
        try {
            return mapper.writerFor(Window.class).writeValueAsBytes(data);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);        }
    }

    @Override
    public void close() {

    }
}
