package jacekolszak;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.PunctuationType;
import org.apache.kafka.streams.processor.Punctuator;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

final class SuppressingTransformer<K, V, R extends KeyValue<K, V>> implements Transformer<K, V, R>, Punctuator {

    private final long windowDurationMillis = 100L;
    private Long windowStart;
    private ProcessorContext context;
    private final Map cache = new HashMap();

    @Override
    public void init(ProcessorContext context) {
        this.context = context;
        context.schedule(Duration.ofMillis(windowDurationMillis), PunctuationType.WALL_CLOCK_TIME, this);
        // TODO find parent node with window and read the window duration.
    }

    private long windowStart(long timeMillis) {
        return (timeMillis / windowDurationMillis) * windowDurationMillis;
    }

    @Override
    public R transform(K key, V value) {
        ensureWindowStartIsSet();
        System.out.println("transforming " + key + ":" + value);
        cache.put(key, value);
        return null;
    }

    private void ensureWindowStartIsSet() {
        if (windowStart == null) {
            windowStart = windowStart(context.timestamp());
        }
    }

    @Override
    public void close() {
    }

    @Override
    public void punctuate(long now) {
        ensureWindowStartIsSet();
        System.out.println("punctuate with t = " + now);
        System.out.println("millis elapsed = " + (now - windowStart));
        if (now - windowStart > windowDurationMillis) {
            System.out.println("forwarding " + cache.size() + " events");
            cache.forEach((key, value) -> context.forward(key, value));
            cache.clear();
            windowStart = windowStart(now);
            System.out.println("starting new window with t=" + windowStart);
        }
    }

}
