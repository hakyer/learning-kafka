package jacekolszak;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.kstream.TransformerSupplier;

final class WindowSuppressionTransformerSupplier<V, R extends KeyValue<Window, V>> implements TransformerSupplier<Window, V, R> {
    private final long windowDurationMillis;

    WindowSuppressionTransformerSupplier(long windowDurationMillis) {
        this.windowDurationMillis = windowDurationMillis;
    }

    @Override
    public Transformer<Window, V, R> get() {
        return new WindowSuppressingTransfomer<>(windowDurationMillis);
    }
}
