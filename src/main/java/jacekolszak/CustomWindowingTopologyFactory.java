package jacekolszak;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.state.KeyValueBytesStoreSupplier;
import org.apache.kafka.streams.state.Stores;

final class CustomWindowingTopologyFactory {

    private final long windowDurationMs = 1000L;
    private final WindowSerde windowSerde = new WindowSerde();
    private final Serde<String> stringSerde = Serdes.String();

    Topology topology() {
        KeyValueBytesStoreSupplier store = Stores.inMemoryKeyValueStore("store");

        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> inputStream = builder.stream("input");
        inputStream
                .filter((key, value) -> key.equals("k1"))
                .map((key, value) -> KeyValue.pair(new Window(System.currentTimeMillis(), windowDurationMs), value))
                .groupByKey(Grouped.with(windowSerde, stringSerde))
                .reduce((value1, value2) -> value1 + value2, Materialized.<Window, String>as(store).withKeySerde(windowSerde).withValueSerde(stringSerde))
                .toStream()
                .transform(new WindowSuppressionTransformerSupplier<>(windowDurationMs))
                .map((key, value) -> KeyValue.pair("index", value))
                .to("output");
        return builder.build();
    }

}
