package jacekolszak;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.test.ConsumerRecordFactory;
import org.junit.jupiter.api.Test;

import java.util.Properties;

import static org.apache.kafka.streams.StreamsConfig.*;
import static org.apache.kafka.streams.StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

final class WindowedTopologyFactoryTest {

    private static final StringSerializer stringSerializer = new StringSerializer();
    private static final StringDeserializer stringDeserializer = new StringDeserializer();

    private static final ConsumerRecordFactory<String, String> recordFactory = new ConsumerRecordFactory<>(stringSerializer, stringSerializer);

    private static Properties config() {
        Properties config = new Properties();
        config.put(APPLICATION_ID_CONFIG, "app1");
        config.put(BOOTSTRAP_SERVERS_CONFIG, "dummy:9092");
        config.put(DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        config.put(DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        config.put(POLL_MS_CONFIG, "10");
        return config;
    }

    @Test
    void should_concatenate_values_in_100ms_window() throws InterruptedException {
        WindowedTopologyFactory factory = new WindowedTopologyFactory();
        Topology topology = factory.topology();

        long now = System.currentTimeMillis();
        try (TopologyTestDriver driver = new TopologyTestDriver(topology, config(), now)) {
            driver.pipeInput(record("k1", "A", now + 10));
            driver.pipeInput(record("k1", "B", now + 20));
            driver.advanceWallClockTime(110L); // window's duration + poll_ms_config
            assertThat(nextOutput(driver).value()).isEqualTo("AB");
            assertThat(nextOutput(driver)).isNull();
        }
    }


    private ProducerRecord<String, String> nextOutput(TopologyTestDriver testDriver) {
        return testDriver.readOutput("output", stringDeserializer, stringDeserializer);
    }

    private static ConsumerRecord<byte[], byte[]> record(String key, String value, long timestamp) {
        return recordFactory.create("input", key, value, timestamp);
    }
}
